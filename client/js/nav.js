function init() {
    var table = document.getElementById('user-frame');
    var username = localStorage.getItem('username');
    var stmt = ['<td><center>Welcome, ' + username + '</center></td>', '<td><center><a href="javascript:logout();">Logout</a></center></td>'];
    var stmt2 = ['<td><center>Welcome, Guest</center></td>', '<td><center><a href="./login.html">Login</a> / <a href="register.html">Register</a></center></td>'];
    var i = 0;
    if (localStorage.username) {
        while (i < 2) {
            var row = document.createElement('tr');
            row.innerHTML = stmt[i];
            table.appendChild(row);
            i++;
        }
    }
    else {
        while (i < 2) {
            var row = document.createElement('tr');
            row.innerHTML = stmt2[i];
            table.appendChild(row);
            i++;
        }
    }

}
init();

function logout() {
    localStorage.removeItem('id');
    localStorage.removeItem('username');
    localStorage.removeItem('email');
    window.location.reload();
}
