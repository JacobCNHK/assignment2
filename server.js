// called firebase

var firebase = require('firebase');
var config = {
  apiKey: "AIzaSyDj6S5mrzAYj0_G9Czh-ANTrQ9vaLPReT0",
  authDomain: "assignment2-restful-api.firebaseapp.com",
  databaseURL: "https://assignment2-restful-api.firebaseio.com",
  projectId: "assignment2-restful-api",
  storageBucket: "assignment2-restful-api.appspot.com",
  messagingSenderId: "681843064676"
};
firebase.initializeApp(config);
var db = firebase.database();

const restify = require("restify");
const server = restify.createServer();

const request = require("request");

var port = process.env.POST || 8080;
var fb = require("./firebase.js");
var weather = require("./weather.js");
var location = require("./location.js");

// Test get the data from firebase
server.get('/test', (req, res) => {
  console.log("test");
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader('Access-Control-Allow-Methods', 'GET');
  res.write("test");
  res.end();
});

// User variable

server.get('/usr/login/:username/:pwd', (req, res) => {
  var username = req.params.username;
  var pwd = req.params.pwd;

  console.log("Username : " + username);
  console.log("Password : " + pwd);

  fb.selectUserByUsername(db, 'username', username, function(snapshot) {
    snapshot.forEach(function(data) {
      res.setHeader("Access-Control-Allow-Origin", "*");
      res.setHeader('Access-Control-Allow-Methods', 'GET');
      if (data.val().pwd == pwd) {
        var object = {
          "isSuccess": true,
          errMsg: "",
          id: data.key,
          username: data.val().username,
          email: data.val().email
        }
        var output = JSON.stringify(object);
        res.write(output);
        res.end();
      }
      else {
        var object = {
          "isSuccess": false,
          errMsg: "The username or password is wrong!",
          id: null,
          username: null,
          email: null
        }
        var output = JSON.stringify(object);
        res.write(output);
        res.end();
      }
    });
  });
});



server.get('/usr/create/:username/:email/:pwd', (req, res) => {
  var username = req.params.username;
  var pwd = req.params.pwd;
  var email = req.params.email;
  var id = 0;

  var obj = {
    username: username,
    email: email,
    pwd: pwd
  };

  fb.usrSize(db, function(snapshot) {
    id = snapshot.numChildren();
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Methods', 'GET');
    if (fb.manageUser(db, id, obj)) {
      res.write(JSON.stringify({ "isSuccess": true }));
    }
    else {
      res.write(JSON.stringify({ "isSuccess": false }));
    }
    res.end();
  });
});

server.get('/usr/update/:id/:username/:email/:pwd', (req, res) => {
  var id = req.params.id;
  var username = req.params.username;
  var email = req.params.email;
  var pwd = req.params.pwd;
  var obj = {
    username: username,
    email: email,
    pwd: pwd
  };

  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader('Access-Control-Allow-Methods', 'GET');
  if (fb.manageUser(db, id, obj)) {
    res.write(JSON.stringify({ "isSuccess": true }));
  }
  else {
    res.write(JSON.stringify({ "isSuccess": false }));
  }
  res.end();
});

server.get('/usr/check/username/:username', (req, res) => {
  var username = req.params.username;
  fb.selectUserByUsername(db, 'username', username, function(snapshot) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Methods', 'GET');
    if (snapshot.val() == null) {
      res.write(JSON.stringify({ "isSuccess": true }));
    }
    else {
      snapshot.forEach(function(data) {
        if (data.val().username == username) {
          res.write(JSON.stringify({ "isSuccess": false }));
        }
      });
    }
    res.end();
  });
});

// Bookmark

server.get('/bookmark/list/:id', (req, res) => {
  var id = req.params.id;
  fb.selectBookmark(db, id, function(snapshot) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Methods', 'GET');
    var output = JSON.stringify(snapshot.val());
    res.write(output);
    res.end();
  });
});

server.get('/bookmark/add/:uid/:city', (req, res) => {
  var uid = req.params.uid;
  var city = req.params.city;
  var id = 0;

  var obj = {
    uid: uid,
    city: city
  };

  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader('Access-Control-Allow-Methods', 'GET');
  if (fb.insertBookmark(db, obj)) {
    var object = {
      isSuccess: true
    };
    var output = JSON.stringify(object);
    res.write(output);
  }
  else {
    var object = {
      isSuccess: false
    };
    var output = JSON.stringify(object);
    res.write(output);
  }
  res.end();
});

server.get('/bookmark/remove/:id', (req, res) => {
  var id = req.params.id;

  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader('Access-Control-Allow-Methods', 'GET');
  if (fb.deleteBookmark(db, id)) {
    res.write(JSON.stringify({ "isSuccess": true }));
  }
  else {
    res.write(JSON.stringify({ "isSuccess": false }));
  }
  res.end();
});

// Weather
server.get('/weather/city/:city', (req, res) => {
  var city = req.params.city;

  weather.getWeatherByCityName(request, city, function(data) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Methods', 'GET');
    if (data.cod == 200) {
      var object = {
        isFound: true,
        city_name: data.name,
        temp: data.main.temp,
        min_temp: data.main.temp_min,
        max_temp: data.main.temp_max,
        icon: data.weather[0].icon,
        main: data.weather[0].main,
        lon: data.coord.lon,
        lat: data.coord.lat
      }
      var output = JSON.stringify(object);
      res.write(output);
    }
    else {
      var object = {
        isFound: false,
        errMsg: data.message
      }
      var output = JSON.stringify(object);
      res.write(output);
    }
    res.end();
  });
});

server.get('/weather/ip/:ip', (req, res) => {
  var ip = req.params.ip;

  location.getLocationByIP(request, ip, function(info) {
    weather.getWeatherByLocation(request, info, function(data) {
      res.setHeader("Access-Control-Allow-Origin", "*");
      res.setHeader('Access-Control-Allow-Methods', 'GET');
      if (data.cod == 200) {
        var object = {
          isFound: true,
          city_name: data.name,
          temp: data.main.temp,
          min_temp: data.main.temp_min,
          max_temp: data.main.temp_max,
          icon: data.weather[0].icon,
          main: data.weather[0].main,
          lon: data.coord.lon,
          lat: data.coord.lat
        }
        var output = JSON.stringify(object);
        res.write(output);
      }
      else {
        var object = {
          isFound: false,
          errMsg: data.message
        }
        var output = JSON.stringify(object);
        res.write(output);
      }
      res.end();
    });
  });
});

server.listen(port, function(err) {
  if (err) {
    console.error(err);
  }
  else {
    console.log('App is ready at : ' + port);
  }
});
