exports.getLocationByIP = function(request, ip, callback) {
    var url = "http://ipapi.co/" + ip + "/json/";
    request(url, function(err, res, body) {
        var info = {};
        if (!err && res.statusCode == 200) {
            let detail = JSON.parse(body)
            info.lon = detail.longitude;
            info.lat = detail.latitude;
        }
        console.log(info.lon + ", "+info.lat)
        callback(info);
    })
}