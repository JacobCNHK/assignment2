// testing method

exports.selectAll = function(db, callback) {
    var ref = db.ref("/");
    ref.once("value", function(snapshot) {
        callback(snapshot);
    });
}

// User Controller
exports.usrSize = function(db, callback) {
    var ref = db.ref("/usr");
    ref.once("value", function(snapshot) {
        callback(snapshot);
    });
}

exports.selectUserByUsername = function(db, type, content, callback) {
    var ref = db.ref("/usr");
    ref.orderByChild(type).equalTo(content).once('value', function(snapshot) {
        callback(snapshot);
    });
};

exports.manageUser = function(db, id, object) { // insert or update user
    var ref = db.ref("/usr");
    if (ref.child(id).set(object)) {
        return true;
    }
    else {
        return false;
    }
}

// Bookmark Controller

exports.selectBookmark = function(db, uid, callback) {
    var ref = db.ref("/bookmark");
    ref.orderByChild('uid').equalTo(uid).once('value', function(snapshot) {
        callback(snapshot);
    })
}

exports.insertBookmark = function(db, object) {
    var ref = db.ref("/bookmark");
    if (ref.push(object)) {
        return true;
    }
    else {
        return false;
    }
}

exports.deleteBookmark = function(db, id) {
    var ref = db.ref("/bookmark/"+id);
    if (ref.remove()) {
        return true;
    } else {
        return false;
    }
}
