var openweathermap_api_key = "a52213f634651e81768f82e0ba9800d5";

exports.getWeatherByLocation = function(request, loca, callback) {
    var url = 'http://api.openweathermap.org/data/2.5/weather?lat=' + loca.lat + '&lon=' + loca.lon + '&appid=' + openweathermap_api_key + '&units=metric';
    request(url, function(err, res, body) {
        let weather = JSON.parse(body);
        console.log(weather);
        callback(weather);
    })
}

exports.getWeatherByCityName = function(request, city, callback) {
    var url = 'http://api.openweathermap.org/data/2.5/weather?q=' + city + '&appid=' + openweathermap_api_key + '&units=metric';
    request(url, function(err, res, body) {
        let weather = JSON.parse(body);
        console.log(weather);
        callback(weather);
    })
}
